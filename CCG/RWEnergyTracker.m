//
//  RWEnergyTracker.m
//  CCG
//
//  Created by Brian Broom on 2/27/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import "RWEnergyTracker.h"
#import "UIColor+CCGColors.h"
#import "RWCard.h"
#import "RWGameScene.h"
#import "RWPlayer.h"
#import "RWStarTracker.h"

@interface RWEnergyTracker ()

@property (strong, nonatomic) NSMutableArray *unusedEnergyCards;
@property (strong, nonatomic) NSMutableArray *usedEnergyCards;
@property (weak, nonatomic, readonly) RWGameScene *gameScene;


@end

@implementation RWEnergyTracker

- (id)init {
    self = [super initWithColor:[UIColor clearColor] size:CGSizeMake(250, 180)];
    if (self) {
        _unusedEnergyCards = [NSMutableArray new];
        _usedEnergyCards = [NSMutableArray new];
        self.name = @"Energy Tracker";
    }
    return self;
}

- (RWGameScene *)gameScene {
    return (RWGameScene *)self.scene;
}

- (NSInteger)availableEnergy {
    return ([self.usedEnergyCards count] + [self.unusedEnergyCards count]);
}

#pragma mark - Energy Card Movement

- (void)addStarFromCard:(RWCard *)card {
    if ([self.starTracker canAddStar]) {
        [self.starTracker addStar];
        [self.unusedEnergyCards removeObject:card];
        [self.usedEnergyCards addObject:card];
        
        // sound effect
        [self runAction:[SKAction playSoundFileNamed:@"activate-energy.wav" waitForCompletion:NO]];
        
        // change texture
        card.texture = [SKTexture textureWithImageNamed:@"card_energy_used.png"];
        card.active = NO;
        card.position = [self positionForUsedCard:card];
    }
}

- (void)resetCards {
    [self.starTracker resetStars];
    for (RWCard *card in self.usedEnergyCards) {
        card.active = YES;
        card.texture = [SKTexture textureWithImageNamed:@"card_energy.png"];
        [self.unusedEnergyCards addObject:card];
        card.position = [self positionForUnusedCard:card];
    }
    [self.usedEnergyCards removeAllObjects];
}

#pragma mark - Card Position Calculations

- (CGPoint)positionForUnusedCard:(RWCard *)card {
    
    if ([self.player.name isEqualToString:@"Druid"]) {
        NSInteger index = [self.unusedEnergyCards indexOfObject:card];
        card.zPosition = index;
        return CGPointMake(67, 292 - 18 * index);
    }
    
    if ([self.player.name isEqualToString:@"Beastmaster"]) {
        NSInteger index = [self.unusedEnergyCards indexOfObject:card];
        card.zPosition = index;
        return CGPointMake(184, 474 + 18 * index);
    }
    
    return CGPointZero;
}

- (CGPoint)positionForUsedCard:(RWCard *)card {
    
    if ([self.player.name isEqualToString:@"Druid"]) {
        NSInteger index = [self.usedEnergyCards indexOfObject:card];
        card.zPosition = index;
        return CGPointMake(184, 292 - 18 * index);
    }
    
    if ([self.player.name isEqualToString:@"Beastmaster"]) {
        NSInteger index = [self.usedEnergyCards indexOfObject:card];
        card.zPosition = index;
        return CGPointMake(67, 474 + 18 * index);
    }
    
    NSLog(@"Player name '%@' did not match.", self.player.name);
    return CGPointZero;
}

#pragma mark - RWCardRegion Protocol Methods

- (BOOL)shouldAddCard:(RWCard *)card fromRegion:(SKNode<RWCardRegion> *)sourceRegion {
    
    return NO;
    
}

- (void)addCard:(RWCard *)card fromRegion:(SKNode<RWCardRegion> *)sourceRegion {
    [self.unusedEnergyCards addObject:card];
    if (sourceRegion) { [sourceRegion removeCard:card]; }
    card.currentRegion = self;
    self.hasPlayedEnergyCardThisTurn = YES;
    
    CGPoint newPosition = [self positionForUnusedCard:card];
    
    [card runAction:[SKAction moveTo:newPosition duration:0.3]];
}

- (BOOL)containsCard:(RWCard *)card {
    return ([self.unusedEnergyCards containsObject:card] || [self.usedEnergyCards containsObject:card]);
}

- (void)removeCard:(RWCard *)card {
    // there is no effect for trying to remove an object not in the array
    [self.unusedEnergyCards removeObject:card];
    [self.usedEnergyCards removeObject:card];
}

- (BOOL)canAttackCardAttackAtPoint:(CGPoint)point {
    return NO;
}

@end
