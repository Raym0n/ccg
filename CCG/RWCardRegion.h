//
//  RWCardRegion.h
//  CCG
//
//  Created by Brian Broom on 2/27/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RWCard;

@protocol RWCardRegion <NSObject>

- (BOOL)shouldAddCard:(RWCard *)card fromRegion:(SKNode<RWCardRegion> *)sourceRegion;
- (void)addCard:(RWCard *)card fromRegion:(SKNode<RWCardRegion> *)sourceRegion;
- (void)removeCard:(RWCard *)card;
- (BOOL)containsCard:(RWCard *)card;


@end
