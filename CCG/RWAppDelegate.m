//
//  RWAppDelegate.m
//  CCG
//
//  Created by Brian Broom on 2/25/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

@import AVFoundation;
#import <Crashlytics/Crashlytics.h>
#import "RWAppDelegate.h"

@implementation RWAppDelegate


// AFAudioPlayer going into the background will crash the app
// solution from http://stackoverflow.com/a/21349677/1042111

static BOOL isAudioSessionActive = NO;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Crashlytics startWithAPIKey:@"dc50ef2caba84b22351812ed99302ed0c820c8a3"];
    [self startAudio];
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{

}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // SpriteKit uses AVAudioSession for [SKAction playSoundFileNamed:]
    // AVAudioSession cannot be active while the application is in the background,
    // so we have to stop it when going in to background
    // and reactivate it when entering foreground.
    // This prevents audio crash.
    [self stopAudio];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [self startAudio];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [[AVAudioSession sharedInstance] setActive:YES withOptions:0 error:nil];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [self stopAudio];
}

- (void)startAudio {
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSError *error = nil;
    
    [audioSession setCategory: AVAudioSessionCategorySoloAmbient error:&error];
    
    if (!error) {
        [audioSession setActive:YES error:&error];
        isAudioSessionActive = YES;
    }
    
    //NSLog(@"%s AVAudioSession Category: %@ Error: %@", __FUNCTION__, [audioSession category], error);
}

- (void)stopAudio {
    // Prevent background apps from duplicate entering if terminating an app.
    if (!isAudioSessionActive) return;
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSError *error = nil;
    
    [audioSession setActive:NO error:&error];
    
    //NSLog(@"%s AVAudioSession Error: %@", __FUNCTION__, error);
    
    if (error) {
        // It's not enough to setActive:NO
        // We have to deactivate it effectively (without that error),
        // so try again (and again... until success).
        [self stopAudio];
    } else {
        isAudioSessionActive = NO;
    }
}

@end
