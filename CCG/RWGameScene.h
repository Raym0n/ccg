//
//  RWGameScene.h
//  CCG
//
//  Created by Brian Broom on 2/25/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "RWCardRegion.h"

@class RWCard;
@class RWPlayer;
@class RWStarTracker;
@class RWPlayerTracker;

@interface RWGameScene : SKScene

@property (copy, nonatomic) NSString *currentPhase;
@property (copy, nonatomic) NSString *subPhase;
@property (assign, nonatomic) NSInteger turnNumber;
@property (weak, nonatomic) RWPlayer *currentPlayer;
@property (weak, nonatomic) RWPlayer *defendingPlayer;
@property (strong, nonatomic) RWCard *spellToCast;
@property (strong, nonatomic) RWStarTracker *starTracker;
@property (strong, nonatomic) RWPlayerTracker *playerTracker;

//- (void)drawCard:(RWCard *)card;

- (void)showLargeCard:(RWCard *)card;
- (void)notifyWithString:(NSString *)message;
- (SKNode<RWCardRegion> *)regionForPoint:(CGPoint)point;
- (void)selectTargetsForSpell:(RWCard *)card;
- (void)cast: (RWCard *)spell onTarget:(RWCard *)target;
- (void)castSpellOnTarget:(RWCard *)target;
- (void)fightWithAttacker:(RWCard *)attacker andDefender:(RWCard *)defender;
- (void)endFight;
- (void)playerDies:(RWPlayer *)player;
- (void)nextTurn;

@end
