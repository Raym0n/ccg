//
//  RWPlayer.h
//  CCG
//
//  Created by Brian Broom on 2/27/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <Foundation/Foundation.h>

@class RWDeck;
@class RWDiscard;
@class RWHand;
@class RWEnergyTracker;
@class RWInPlayRegion;

@interface RWPlayer : NSObject
@property (copy, nonatomic) NSString *name;
@property (assign, nonatomic) CGFloat rotation;

@property (assign, nonatomic) BOOL active;

@property (strong, nonatomic) SKNode *turnNotifyOverlay;
@property (strong, nonatomic) RWDeck *deck;
@property (strong, nonatomic) RWDiscard *discard;
@property (strong, nonatomic) RWHand *hand;
@property (strong, nonatomic) RWEnergyTracker *energyTracker;
@property (strong, nonatomic) RWInPlayRegion *inPlay;

//@property (strong, nonatomic, readonly) NSArray *regions;

- (void)makeActive;
- (void)makeInactive;

@end
