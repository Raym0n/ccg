//
//  RWDiscard.m
//  CCG
//
//  Created by Brian Broom on 2/27/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import "RWDiscard.h"
#import "RWCard.h"
#import "RWGameScene.h"
#import "UIColor+CCGColors.h"

@interface RWDiscard ()

@property (strong, nonatomic) SKSpriteNode *blankCard;
@property (strong, nonatomic) NSMutableArray *cards;

@end

@implementation RWDiscard

- (id)init {
    self = [super initWithColor:[UIColor clearColor] size:CGSizeMake(100, 140)];
    if (self) {
        _cards = [NSMutableArray new];
    }
    return self;
}


#pragma mark - RWCardRegion Protocol Methods

- (BOOL)shouldAddCard:(RWCard *)card fromRegion:(SKNode<RWCardRegion> *)sourceRegion {
    if (card.player == self.player) {
        return YES;
    } else {
        [(RWGameScene *)self.scene notifyWithString:@"You may only place your cards in your discard pile"];
        return NO;
    }
}

- (void)addCard:(RWCard *)card fromRegion:(SKNode<RWCardRegion> *)sourceRegion {
    [card discardAllAttachemnts];
    
    [self.cards addObject:card];
    if (sourceRegion) { [sourceRegion removeCard:card]; }
    card.currentRegion = self;
    
    [card runAction:[SKAction moveTo:self.position duration:0.3]];
    self.texture = card.texture;
    [card removeFromParent];
}

- (BOOL)containsCard:(RWCard *)card {
    return [self.cards containsObject:card];
}

- (void)removeCard:(RWCard *)card {
    if ([self containsCard:card]) {
        [self.cards removeObject:card];
    }
}

- (BOOL)canAttackCardAttackAtPoint:(CGPoint)point {
    return NO;
}

@end
