//
//  RWLargeCardOverlay.h
//  CCG
//
//  Created by Brian Broom on 2/27/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@class RWCard;

@interface RWLargeCardOverlay : SKSpriteNode

- (void)displayLargeCard:(RWCard *)card andRotation:(CGFloat)rotation;

@end
