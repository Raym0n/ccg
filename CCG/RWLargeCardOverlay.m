//
//  RWLargeCardOverlay.m
//  CCG
//
//  Created by Brian Broom on 2/27/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import "RWLargeCardOverlay.h"
#import "RWCard.h"

@interface RWLargeCardOverlay ()

@property (strong, nonatomic) SKSpriteNode *largeCard;
@property (weak, nonatomic) RWCard *smallCard;

@end

@implementation RWLargeCardOverlay

- (id)initWithColor:(UIColor *)color size:(CGSize)size {
    self = [super initWithColor:[UIColor grayColor] size:size];
    if (self) {
        self.zPosition = -20;
        self.userInteractionEnabled = YES;
        _largeCard = nil;    }
    return self;
}

#pragma mark - Overlay

- (void)displayLargeCard:(RWCard *)card andRotation:(CGFloat)rotation {
    // clean up large card sprite from previous displays
    [self removeAllChildren];
    
    self.smallCard = card;
    self.zRotation = rotation;
    self.largeCard = [SKSpriteNode spriteNodeWithImageNamed:card.largeCardFileName];
    
    // scale and position of small card
    [self.largeCard setScale:0.364];
    self.largeCard.position = [self convertPoint:self.smallCard.position fromNode:self.smallCard.scene];
    
    [self addChild:self.largeCard];
    self.zPosition = 20;
    
    // enlarge animation
    CGFloat endScale = 2.0;
    
    SKAction *center = [SKAction moveTo:CGPointZero duration:0.3];
    SKAction *enlarge = [SKAction scaleTo:endScale duration:0.3];
    SKAction *centerEnlarge = [SKAction group:@[center, enlarge]];
    
    [self.largeCard runAction:centerEnlarge];
    
}

- (void)dismissDisplay {
    // back to original point
    CGPoint oldPosition = [self convertPoint:self.smallCard.position fromNode:self.smallCard.scene];
    CGFloat oldScale = 0.364;
    
    SKAction *move = [SKAction moveTo:oldPosition duration:0.3];
    SKAction *shrink = [SKAction scaleTo:oldScale duration:0.3];
    SKAction *returnShrink = [SKAction group:@[move, shrink]];
    
    [self.largeCard runAction:returnShrink completion:^{
        self.smallCard = nil;
        self.largeCard = nil;
        self.zPosition = -20;
    }];
    
    
}

#pragma mark - Touch Handlers

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self dismissDisplay];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
}


@end
